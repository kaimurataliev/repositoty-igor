import React, { Component, Fragment } from 'react';
import './App.css';
import {Switch, Route} from 'react-router-dom';
import MainPage from "./containers/MainPage/MainPage";
import Login from "./containers/Login/Login";


class App extends Component {
  render() {
    return (
          <Switch>
            <Route path="/" exact component={Login}/>
            <Route path="/info" component={MainPage}/>
          </Switch>
    );
  }
}

export default App;
