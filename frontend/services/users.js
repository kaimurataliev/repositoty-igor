import axios from 'axios';

export const getCurrentUser = async () => {
  // const token = localStorage.getItem('token');
  // await axios.get('/users/byToken', {headers: {Authorization: "Bearer " + token}});

  const user = {
    id: 112,
    power: 15000,
    balance: 51021.5,
    firstName: 'Айбек',
    lastName: 'Аилчиев',
    email: 'crypton.kg@gmail.com',
    pincode: '1213',
    avatar: 'https://aiconsult.co/wp-content/uploads/2013/01/Aibek-Ailchiev.jpg'
  }

  return user
}