import axios from 'axios';

export const calculateDailyProfit = (power) => {
  return axios.get('https://whattomine.com/coins/151.json')
    .then(function (rawData) {
      const data = rawData.data;
      const t = (((power * data.block_reward) / (data.difficulty / 1000000)) * (1 - 0.15) * 3600 * 24) * 645;
      return t;
    })
    .catch(function (error) {
      console.log(error);
    });

};