import React, { Component } from 'react';
import './MainPage.css';
import Balance from '../../components/Balance/Balance';
import Power from '../../components/Power/Power';
import Income from '../../components/Income/Income';
import Payout from '../../components/Payout/Payout';
import Graph from '../../components/Graph/Graph';
import BuyMorePower from '../../components/BuyMorePower/BuyMorePower';
import { connect } from 'react-redux';
import { setCurrentUser } from '../../store/actions';
import {getCurrentUser, getUser} from "../../services/users";
import Header from '../../components/HeaderComponent/HeaderComponent'

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.updateUser();
    // setInterval(this.updateUser, 5000);
  }

  updateUser = async () => {
    const user = await getCurrentUser();
    this.props.setCurrentUser(user);
  };

  state = {
    activeItem: 'graph'
  };

  changeActiveItem = (activeItem) => () => {
    this.setState({
      activeItem
    });
  };

  render() {
    return (
      <div className="container-page">
        <Header avatar={this.props.user.avatar} firstName={this.props.user.firstName} lastName={this.props.user.lastName}/>
        <div className="container">
          <div className="items">
            <Balance showPayout={this.changeActiveItem('payout')} balance={this.props.user.balance}/>
            <Power showBuyMore={this.changeActiveItem('buy')} power={this.props.user.power}/>
            <Income power={this.props.user.power}/>
          </div>

          <Graph display={this.state.activeItem === 'graph'}/>
          <Payout display={this.state.activeItem === 'payout'} showGraph={this.changeActiveItem('graph')}/>
          <BuyMorePower display={this.state.activeItem === 'buy'} showGraph={this.changeActiveItem('graph')}/>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return { user: state.user };
};

const mapDispatchToProps = (dispatch) => (
  {
    setCurrentUser: setCurrentUser(dispatch)
  }
);


export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
