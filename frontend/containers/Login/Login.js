import React, {Component} from 'react';
import {withRouter, Redirect} from 'react-router-dom';
import './Login.css';
import { doSignInWithEmailAndPassword } from '../../services/auth';
import logo from './logo.png';

const SignInPage = (props) =>
  <div className="login-page">
    <SignInForm {...props}/>
  </div>

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInForm extends Component {

  state = {
    redirect: false
  }

  constructor(props) {
    super(props);

    this.state = {...INITIAL_STATE};
  }

  onSubmit = (event) => {

    event.preventDefault();

    const {
      email,
      password,
    } = this.state;

    doSignInWithEmailAndPassword(email, password)
      .then(async (token) => {
        localStorage.setItem('token', token);
        this.setState({redirect: true})
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
  }

  render() {
    const {redirect} = this.state;

    if (redirect) {
      return <Redirect to='/info'/>;
    }

    const {
      email,
      password,
      error,
    } = this.state;

    const isInvalid =
      password === '' ||
      email === '';


    return (
      <div className="login-form">
        <div className="top-line"></div>
        <img src={logo} alt=""/>
        <form onSubmit={this.onSubmit}>
          <h4>E-mail</h4>
          <input
            value={email}
            onChange={event => this.setState(byPropKey('email', event.target.value))}
            type="text"
            placeholder="Почтовый адрес"
          /><br/>
          <h4>Пароль</h4>
          <input
            value={password}
            onChange={event => this.setState(byPropKey('password', event.target.value))}
            type="password"
            placeholder="Ваш пароль"
          /><br/>
          {error && <p>{error}</p>}
          <button disabled={isInvalid} type="submit">
            Войти
          </button>

        </form>
      </div>

    );
  }
}


export default withRouter(SignInPage)
