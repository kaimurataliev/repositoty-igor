import React from 'react'
import './PayoutCashModal.css'
import {Button, Header, Icon, Modal, Input} from 'semantic-ui-react';

class PayoutCashModal extends React.Component {
  state = {
    modalOpen: false,
    isSuccess: false,
    title: 'Вывод средств наличными',
    message: 'showForm'
  };

  handleOpen = () => {
    this.setState({
      modalOpen: true,
      isSuccess: false,
      title: 'Вывод средств наличными',
      message: 'showForm'
    })

  };

  handleClose = () => this.setState({modalOpen: false});

  showSuccess = () => {
    this.setState({
      title: 'Вывод средств',
      message: 'Заявка на вывод средств принята и будет рассмотрена в течение 24 часов',
      isSuccess: !this.state.isSuccess
    });
  };

  render() {
    return (
      <Modal trigger={<div className="payout-button cash" onClick={this.handleOpen}>Вывести наличными</div>}
             open={this.state.modalOpen}
             size='tiny'>
        <Header icon='payment' content={this.state.title}/>
        <Modal.Content>
          {this.state.message === 'showForm' ?
            <div>
              <Input size='large' icon='money' iconPosition='left' placeholder='Сумма вывода'
                     className='modal-input'/><br/>
              <Input size='large' icon='unlock alternate' iconPosition='left' placeholder='Пин код'
                     className='modal-input'/>
            </div> : this.state.message
          }
        </Modal.Content>
        {this.state.isSuccess ?
          <Modal.Actions><Button color='green' inverted onClick={this.handleClose}>
            <Icon name='checkmark'/> Ок
          </Button></Modal.Actions> :
          <Modal.Actions><Button basic color='red' onClick={this.handleClose}>
            <Icon name='remove'/> Отмена
          </Button>
            <Button color='green' inverted onClick={this.showSuccess}>
              <Icon name='checkmark'/> Подтвердить
            </Button>
          </Modal.Actions>
        }
      </Modal>
    );
  }

}

export default PayoutCashModal