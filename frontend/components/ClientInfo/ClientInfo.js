import React from 'react';
import './ClientInfo.css';

const ClientInfo = props => {
    return (
        <div className="client-info">
            <div className="client-id">Клиент #164</div>
            <div className="date">31.03.2018</div>
        </div>
    )
};

export default ClientInfo;