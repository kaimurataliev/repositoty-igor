import React from 'react';
import './Balance.css';

class Balance extends React.Component {

  constructor(props) {
    super(props)
    this.updateLocalStorageBalance();
  }

  setBalanceTOLocalStorage = (props) => {
    !localStorage.getItem('balance') ?
      localStorage.setItem('balance', props.balance) :
      localStorage.setItem('balance', (parseFloat(localStorage.getItem('balance'))).toFixed(2))
  }

  updateBalanceTOLocalStorage = () => {
    if (parseInt(localStorage.getItem('balance'))) {
      localStorage.setItem('balance', (parseFloat(localStorage.getItem('balance')) + 0.0075).toFixed(2));
      this.forceUpdate()
    }
  }

  updateLocalStorageBalance = () => {
    setInterval(() => this.updateBalanceTOLocalStorage(), 2000)
  };

  componentWillReceiveProps(newProps) {
    if (newProps.balance) {
      this.setBalanceTOLocalStorage(newProps);
    }
  }

  render() {
    return (
      <div className="block">
        <div className="block-top-line"></div>
        <div className="block-title">Текущий баланс</div>
        <div className="block-value">${localStorage.balance} <span className="balance-difference">+$674 / 24ч.</span>
        </div>
        <div className="block-link" onClick={this.props.showPayout}>вывести средства</div>
      </div>
    )
  }
};

export default Balance;