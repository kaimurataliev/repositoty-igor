import React from 'react';
import InteractiveGraph from '../InteractiveGraphComponent/App';
import graphimage from './graph.svg'
import './Graph.css'

const Graph = props => {
  if (!props.display) {
    return null
  }
  return (
    <div className="graph-wrapper" style={{display: props.display}}>
      <div className="graph">
        <div className="eth-info">
          <div className="graph-current-text">Стоимость ETH:</div>
          <div className="graph-current-price">$635.26</div>
        </div>
        <img src={graphimage} alt=""/>
        {/*<InteractiveGraph />*/}
      </div>
    </div>

  )
};

export default Graph;