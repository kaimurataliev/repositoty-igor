import React from 'react';
import './BuyMorePower.css';
import {Icon} from 'semantic-ui-react';
import BuyMoreItem from "./BuyMoreItem/BuyMoreItem";

const BuyMorePower = props => {
  if (!props.display) {
    return null
  }
  return (
    <div className="display-block-wrapper" style={{display: props.display}}>
      <div className="buy-more-block">
        <div className="top-row">
          <div className="buy-more-title">Контракты</div>
          <div className="close-button" onClick={props.showGraph}>
            <Icon name="close"/>
          </div>
        </div>
        <div className="buy-more-items">
          <BuyMoreItem price={14990} power={1000} duration={12}/>
          <BuyMoreItem price={56990} power={1500} duration={24}/>
          <BuyMoreItem price={126990} power={5000} duration={36}/>
        </div>
        <span className="buy-more-text">
          <br />
          Равным образом консультация с широким активом позволяет выполнять важные задания по разработке модели развития. С другой стороны реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации соответствующий условий активизации.
          <br /><br />
          Значимость этих проблем настолько очевидна, что реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации новых предложений. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности способствует подготовки и реализации соответствующий условий активизации.
        </span>
      </div>
    </div>
  )
};

export default BuyMorePower;