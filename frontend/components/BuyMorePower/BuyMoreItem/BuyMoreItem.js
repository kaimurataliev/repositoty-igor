import React from 'react';
import './BuyMoreItem.css';
import BuyModal from '../../BuyModal/BuyModal'

const BuyMoreItem = props => {
  return (
    <div className="buy-more-item">
      <div className="top-line"></div>
      <div className="buy-more-row">
        <h3>Мощность</h3>
        <h1>{props.power} MH/s</h1>
      </div>
      <div className="buy-more-row">
        <h3>Срок</h3>
        <h2>{props.duration} мес.</h2>
      </div>
      <div className="buy-more-row">
        <h3>Цена</h3>
        <h2>${props.price}</h2>
      </div>
      <BuyModal />
    </div>
  )
};

export default BuyMoreItem;