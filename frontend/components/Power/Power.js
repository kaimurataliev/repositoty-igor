import React from 'react';
import './Power.css';

const Power = ({showBuyMore, power}) => {
    return (
        <div className="block">
            <div className="block-top-line"></div>
            <div className="block-title">Мощности в работе</div>
            <div className="block-value">{power} MH/s</div>
            <div className="block-link" onClick={showBuyMore}>купить больше</div>
        </div>
    )
};

export default Power;