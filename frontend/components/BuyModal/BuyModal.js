import React from 'react'
import {Button, Header, Icon, Modal} from 'semantic-ui-react';

class BuyModal extends React.Component {
  state = {
    modalOpen: false,
    isSuccess: false,
    title: 'Покупка нового контракта',
    message: 'Вы уверены что хотите приобрести этот контракт?'
  };

  handleOpen = () => {
    this.setState({
      modalOpen: true,
      isSuccess: false,
      title: 'Покупка нового контракта',
      message: 'Вы уверены что хотите приобрести этот контракт?'
    })

  };

  handleClose = () => this.setState({modalOpen: false});

  showSuccess = () => {
    this.setState({
      title: 'Покупка нового контракта',
      message: 'Заявка на приобретение контракта будет рассмотрена в течение 24 часов',
      isSuccess: !this.state.isSuccess
    });
  };

  render() {
    return (
      <Modal trigger={<div className="button-block" onClick={this.handleOpen}>Купить</div>}
             open={this.state.modalOpen}
             onClose={this.handleClose}
             basic size='tiny'>
        <Header icon='payment' content={this.state.title}/>
        <Modal.Content>
          <p>{this.state.message}</p>
        </Modal.Content>

        {this.state.isSuccess ?
          <Modal.Actions><Button color='green' inverted onClick={this.handleClose}>
            <Icon name='checkmark'/> Ок
          </Button></Modal.Actions> :
          <Modal.Actions><Button basic color='red' inverted onClick={this.handleClose}>
            <Icon name='remove'/> Нет
          </Button>
            <Button color='green' inverted onClick={this.showSuccess}>
              <Icon name='checkmark'/> Да
            </Button></Modal.Actions>
        }


      </Modal>
    );
  }

}

export default BuyModal