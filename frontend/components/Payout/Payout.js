import React from 'react';
import './Payout.css';
import {Input, Button, Grid, Icon} from 'semantic-ui-react'
import PayoutWalletModal from './../PayoutWalletModal/PayoutWalletModal'
import PayoutCashModal from "../PayoutCashModal/PayoutCashModal";

const transactions = [
  {
    sum: 500,
    date: '27 апреля 2018, 20:08',
    status: 'обработка',
    auto: '',
  },
  {
    sum: 500,
    date: '23 апреля 2018, 14:09',
    status: 'завершено',
    auto: '',
  },
  {
    sum: 1490,
    date: '20 апреля 2018, 08:51',
    status: 'завершено',
    auto: '',
  },
  {
    sum: 1000,
    date: '14 апреля 2018, 09:03',
    status: 'завершено',
    auto: '',
  },
];

const listItems = transactions.map((transaction) =>
  <div className="transaction">
    <Grid>
      <Grid.Row>
        <Grid.Column width={7}>
          <div className="transaction-sum">${transaction.sum}</div>
        </Grid.Column>
        <Grid.Column width={2}>
          <div className="is-auto">{transaction.auto}</div>
        </Grid.Column>
        <Grid.Column width={4}>
          <div className="date">{transaction.date}</div>
        </Grid.Column>
        <Grid.Column width={3}>
          <div className="status">статус: <span className={transaction.status==='завершено'? 'done' : ''}>{transaction.status}</span></div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);

const listItemsMobile = transactions.map((transaction) =>
  <div className="transaction">
    <Grid>
      <Grid.Row>
        <Grid.Column width={5}>
          <div className="transaction-sum">${transaction.sum}</div>
        </Grid.Column>
        <Grid.Column width={11}>
          <div className="status">статус: <span className={transaction.status==='завершено'? 'done' : ''}>{transaction.status}</span></div>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={13}>
          <div className="date">{transaction.date}</div>
        </Grid.Column>
        <Grid.Column width={3}>
          <div className="is-auto">{transaction.auto}</div>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </div>
);

const Payout = props => {
  if (!props.display) {
    return null
  }
  return (
    <div className="display-block-wrapper" style={{display: props.display}}>
      <div className="payout-block">
        <div className="top-row">
          <div className="payout-title-payout">Вывод средств</div>
          <div className="close-button" onClick={props.showGraph}>
            <Icon name="close"/>
          </div>
        </div>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className="payout-buttons">
                <PayoutWalletModal/>
                <PayoutCashModal/>
              </div>
              {/*<div className="auto-payout">Автоматический вывод средств каждые 72 часа</div>*/}
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <div className="payout-title">История транзакций</div>
              <div className="transactions">
                {listItems}
              </div>
              <div className="transactions-mobile">
                {listItemsMobile}
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    </div>
  )
};

export default Payout;