import React from 'react';
import './HeaderComponent.css';
import logo from './logo.png';

const Header = props => {
  return (
    <div className="header-component">
      <img className="logo" src={logo} alt=""/>
      <div className="user-info">
        <div className="user-email">{props.lastName} {props.firstName}</div>
        <img src={props.avatar} alt=""/>
      </div>

    </div>
  )
};

export default Header;