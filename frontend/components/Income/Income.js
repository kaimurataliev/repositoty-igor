import React from 'react';
import './Income.css';
import ReactTimeout from 'react-timeout'
import {calculateDailyProfit} from '../../services/dailyprofit'

class Income extends React.Component {
  state = {
    value: 660.88
  };

  async componentDidMount() {
    // this.getProfit();
  };

  updateProfit(power) {
    calculateDailyProfit(power)
      .then((dailyProfit) => {
        this.setState({value: dailyProfit.toFixed(2)})
      })
  }

  componentWillReceiveProps(newProps) {
    if (newProps.power) {
      this.updateProfit(newProps.power)
    }
  }

  getProfit = () => {
    setInterval(async () => this.setState({value: await calculateDailyProfit(this.props.power)}), 50000)
  };

  render() {
    return (

      <div className="block">
        <div className="block-top-line"></div>
        {this.currency}
        <div className="block-title">Доход за сутки</div>
        <div className="block-value">${this.state.value}</div>
        {/*<div className="timer">до выплаты 11ч. 29м.</div>*/}
      </div>
    );
  }
}

export default ReactTimeout(Income)