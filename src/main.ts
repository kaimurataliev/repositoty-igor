import { NestFactory, Reflector } from '@nestjs/core'
import { ApplicationModule } from './app.module'
import { RolesGuard } from './guards/roles.guard'

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule)
  app.useGlobalGuards(new RolesGuard(new Reflector()))
  await app.listen(5000)
}

bootstrap()
