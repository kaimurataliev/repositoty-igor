import * as passport from 'passport'
import * as c from 'config'
import { ExtractJwt, Strategy } from 'passport-jwt'
import { Component, UnauthorizedException } from '@nestjs/common'

@Component()
export class JwtStrategy extends Strategy {
  constructor() {
    super(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        passReqToCallback: true,
        secretOrKey: c.get('authentication.jwtSecret')
      },
      async (req, payload, next) => await this.verify(req, payload, next)
    )
    passport.use(this)
  }

  public async verify(req, payload, done) {
    req.user = payload
    const isValid = true
    if (!isValid) {
      return done(new UnauthorizedException(), false)
    }
    done(null, payload)
  }
}