import * as bcrypt from 'bcrypt'
import * as c from 'config'
import * as jwt from 'jsonwebtoken'
import UserModel from '../entities/user.entity'
import { UserJwtPayload } from '../dto/user.dto'

export const hashPassword = (password) => {
  return bcrypt.hash(password, c.get('authentication.saltRounds'))
}

export const comparePasswords = (password: string, hash: string): Promise<boolean> => {
  return bcrypt.compare(password, hash)
}

export const getJwtPayload = (user: UserModel): UserJwtPayload => {
  const { id, email, role } = user
  return { id, email, role }
}

export const signJwt = (payload: any): string => {
  const expiresIn: string = c.get('authentication.expiresMinutes')
  return jwt.sign(payload, c.get('authentication.jwtSecret'), { expiresIn })
}

export const transformAuthRoutes = (authRoutes) =>
  Object.keys(global.authRoutes).reduce((acc, cur) => {
    const controllerRoutes = global.authRoutes[cur].routes.map(route => {
      route.path = `${global.authRoutes[cur].prefix}${route.path}`
      return route
    })
    return acc.concat(controllerRoutes)
  }, [])
