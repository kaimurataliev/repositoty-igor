import UserModel from '../entities/user.entity'

export interface CreateTransactionDto {
  amount: number
  payoutType: string
  userId: number
}

export interface RequestNewTransactionDto {
  amount: number
  payoutType: string
  userId: number
}

export interface TransactionDto {
  amount: number
  date: Date
  status: string
  isAuto: boolean
  payoutType: string
  user: UserModel
}