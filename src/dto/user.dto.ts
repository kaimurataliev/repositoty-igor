import { IsNotEmpty, IsString } from 'class-validator'

export class UpdateUserDto {
  email: string
  power: number
  balance: number
  pincode: number
  avatar: string
  autoPayoutAmount: number
  isAdmin: boolean
}

export class UserDto {
  id: number
  email: string
  power: number
  balance: number
  pincode: number
  avatar: string
  autoPayoutAmount: number
}

export class UserJwtPayload {
  id: number

  email: string

  role: string
}

export class UserAuthenticateDto {
  @IsNotEmpty()
  @IsString()
  email: string

  @IsNotEmpty()
  @IsString()
  password: string
}

export class UserRegisterDto {
  @IsNotEmpty()
  @IsString()
  email: string

  @IsNotEmpty()
  @IsString()
  password: string
}
