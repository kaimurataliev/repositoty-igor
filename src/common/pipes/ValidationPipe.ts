import { ArgumentMetadata, HttpException, HttpStatus, Pipe, PipeTransform } from '@nestjs/common'
import { validate } from 'class-validator'
import { plainToClass } from 'class-transformer'

@Pipe()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value, metadata: ArgumentMetadata) {
    const { metatype } = metadata
    if (!metatype || !this.toValidate(metatype)) {
      return value
    }
    const object = plainToClass(metatype, value)
    const errors = await validate(object)
    if (errors.length > 0) {
      const validationErrorMessage = errors.map((error) => {
        if (!error.constraints || !error.property) {
          return
        }

        return Object.keys(error.constraints)
                     .map(errorKey => error.constraints[errorKey])
                     .join(', ')
      })
      throw new HttpException(validationErrorMessage, HttpStatus.BAD_REQUEST)
    }
    return value
  }

  private toValidate(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object]
    return !types.find(type => metatype === type)
  }
}