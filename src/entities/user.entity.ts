import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('user')
export default class UserModel {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ length: 150, unique: true, nullable: false })
  email: string

  @Column({ nullable: false })
  password: string

  @Column()
  power: number

  @Column({ type: 'decimal' })
  balance: number

  @Column()
  pincode: number

  @Column()
  avatar: string

  @Column({ default: 100 })
  autoPayoutAmount: number

  @Column()
  role: string
}