import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import UserModel from './user.entity'

@Entity('transaction')
export default class TransactionModel {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'decimal' })
  amount: number

  @Column('timestamp with time zone')
  date: Date

  @Column()
  status: string

  @Column()
  isAuto: boolean

  @Column()
  payoutType: string

  @ManyToOne(type => UserModel)
  user: UserModel
}