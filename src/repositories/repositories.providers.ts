import { Connection } from 'typeorm'
import Providers from '../constants/providers'
import { DefaultUserRepository } from './user.repository'
import { DefaultTransactionRepository } from './transaction.repository'

export const providers = [
  {
    provide: Providers.userRepository,
    useFactory: (connection: Connection) => connection.getCustomRepository(DefaultUserRepository),
    inject: [Providers.dbConnection]
  },
  {
    provide: Providers.transactionRepository,
    useFactory: (connection: Connection) => connection.getCustomRepository(DefaultTransactionRepository),
    inject: [Providers.dbConnection]
  }
]