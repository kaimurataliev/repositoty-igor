import { EntityRepository, Repository } from 'typeorm'
import TransactionModel from '../entities/transaction.entity'
import { TransactionDto } from '../dto/transaction.dto'

@EntityRepository(TransactionModel)
export class DefaultTransactionRepository extends Repository<TransactionModel> {
  getTransactionsByUserId(userId: number): Promise<TransactionModel[]> {
    return this.find({ where: { userId } })
    // return this.createQueryBuilder('transaction')
    //            .where('transaction."userId" = :userId', { userId })
    //            .getMany()
  }

  createNewTransaction(transactionData: TransactionDto): Promise<TransactionModel> {
    const transaction = this.create(transactionData)
    return this.save(transaction)
  }
}