import { EntityRepository, Repository } from 'typeorm'
import UserModel from '../entities/user.entity'
import { UpdateUserDto, UserRegisterDto } from '../dto/user.dto'
import UserRoles from '../constants/user-roles'

@EntityRepository(UserModel)
export class DefaultUserRepository extends Repository<UserModel> {
  findById(id: number): Promise<UserModel | undefined> {
    return this.findOne(id)
  }

  findByEmail(email: string): Promise<UserModel | undefined> {
    return this.findOne({ email })
  }

  createNewUser(userData: UserRegisterDto): Promise<UserModel> {
    const userModel = this.create(userData)
    return this.save(userModel)
  }

  async updateUser(id, userDto: UpdateUserDto): Promise<UserModel> {
    await this.update({ id }, {
      email: userDto.email,
      power: userDto.power,
      balance: userDto.balance,
      pincode: userDto.pincode,
      avatar: userDto.avatar,
      autoPayoutAmount: userDto.autoPayoutAmount,
      role: userDto.isAdmin ? UserRoles.ADMIN : UserRoles.USER
    })
    return this.findById(id)
  }

}