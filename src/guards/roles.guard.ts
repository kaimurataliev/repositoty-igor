import { CanActivate, ExecutionContext, Guard } from '@nestjs/common'
import { Observable } from 'rxjs/Observable'
import { Reflector } from '@nestjs/core'

@Guard()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {
  }

  canActivate({ user }, context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const { handler } = context
    const roles = this.reflector.get<string[]>('roles', handler)
    if (!roles) {
      return true
    }
    if (!user || !user.role) {
      return false
    }
    return roles.indexOf(user.role) !== -1
  }

}