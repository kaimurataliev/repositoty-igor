import { AuthControllerRoute } from '../decorators/auth-method.decorator'

declare global {
  namespace NodeJS {
    interface Global {
      authRoutes: { [name: string]: AuthControllerRoute }
    }
  }
}