export default class Providers {

  static dbConnection = 'DbConnection'

  static userRepository = 'userRepository'

  static transactionRepository = 'transactionRepository'
}