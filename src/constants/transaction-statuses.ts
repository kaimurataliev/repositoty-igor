export default class TransactionStatuses {
  static requested = 'requested'

  static inProgress = 'inProgress'

  static completed = 'completed'
}