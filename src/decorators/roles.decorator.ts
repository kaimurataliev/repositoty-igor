import { ReflectMetadata } from '@nestjs/common'

export const RequireRoles = (...roles: string[]) => ReflectMetadata('roles', roles)