import 'reflect-metadata'
import { Controller, Delete, Get, Patch, Post, Put, RequestMethod } from '@nestjs/common'


const authRoutes: { [name: string]: AuthControllerRoute } = {}
global.authRoutes = authRoutes


interface AuthRoute {
  path: string,
  method: RequestMethod
}

export interface AuthControllerRoute {
  routes: AuthRoute[]
  prefix: string
}

const makeAuthRoute = (method: RequestMethod, methodDecorator) => (path: string = '/') => (classInstance, key, descriptor) => {
  if (!authRoutes[classInstance.constructor.name]) {
    authRoutes[classInstance.constructor.name] = { routes: [], prefix: '/' }
  }
  authRoutes[classInstance.constructor.name].routes.push({
                                                           path,
                                                           method: method
                                                         })
  return methodDecorator(path)(classInstance, key, descriptor)
}

export const AuthController = (prefix: string): any => {
  return (classInstance, ...args) => {
    authRoutes[classInstance.name].prefix = prefix
    return Controller(prefix)(classInstance)
  }
}

export const AuthPost = makeAuthRoute(RequestMethod.POST, Post)

export const AuthGet = makeAuthRoute(RequestMethod.GET, Get)

export const AuthPut = makeAuthRoute(RequestMethod.PUT, Put)

export const AuthDelete = makeAuthRoute(RequestMethod.DELETE, Delete)

export const AuthPatch = makeAuthRoute(RequestMethod.PATCH, Patch)