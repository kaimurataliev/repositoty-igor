import { Body, Param, Req } from '@nestjs/common'
import { UserJwtPayload } from '../dto/user.dto'
import { AuthController, AuthGet, AuthPost, AuthPut } from '../decorators/auth-method.decorator'
import DefaultTransactionService from '../services/transaction.service'
import { CreateTransactionDto, RequestNewTransactionDto } from '../dto/transaction.dto'
import { RequireRoles } from '../decorators/roles.decorator'
import UserRoles from '../constants/user-roles'

@AuthController('transactions')
export class TransactionController {

  constructor(private readonly transactionService: DefaultTransactionService) {
  }

  @AuthGet('/')
  findByToken(@Req() req) {
    const userJwt = req.user as UserJwtPayload
    return this.transactionService.findByUserId(userJwt.id)
  }

  @AuthGet('/:userId')
  findByUserId(@Param('userId') userId) {
    return this.transactionService.findByUserId(userId)
  }

  @AuthPost('/doRequestNewTransaction')
  requestNewTransaction(@Body() dto: RequestNewTransactionDto) {
    return this.transactionService.requestNewTransaction(dto)
  }

  @AuthPut('/')
  @RequireRoles(UserRoles.ADMIN)
  createNewTransaction(@Body() dto: CreateTransactionDto) {
    return this.transactionService.createNewTransaction(dto)
  }


}