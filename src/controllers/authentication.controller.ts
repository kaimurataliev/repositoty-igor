import 'reflect-metadata'
import { BadRequestException, Body, Controller, Post } from '@nestjs/common'
import AuthenticationService from '../services/authentication.service'
import { UserAuthenticateDto, UserRegisterDto } from '../dto/user.dto'
import { ValidationPipe } from '../common/pipes/ValidationPipe'

@Controller('auth')
export class AuthenticationController {

  constructor(private readonly authenticationService: AuthenticationService) {
  }

  @Post('/register')
  async doRegistration(@Body(new ValidationPipe()) userRegisterDto: UserRegisterDto) {
    if (!userRegisterDto) {
      throw new BadRequestException('User data is missing!')
    }
    return this.authenticationService.register(userRegisterDto)
  }

  @Post('/')
  async doAuthentication(@Body(new ValidationPipe()) userAuthenticateDto: UserAuthenticateDto) {
    if (!userAuthenticateDto) {
      throw new BadRequestException('User data is missing!')
    }
    return this.authenticationService.authenticate(userAuthenticateDto)
  }
}