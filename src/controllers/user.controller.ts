import { Body, Param, Req } from '@nestjs/common'
import UserService from '../services/user.service'
import { UpdateUserDto, UserJwtPayload } from '../dto/user.dto'
import { AuthController, AuthGet, AuthPost } from '../decorators/auth-method.decorator'

@AuthController('users')
export class UserController {

  constructor(private readonly userService: UserService) {
  }

  @AuthGet('/')
  findByToken(@Req() req) {
    const userJwt = req.user as UserJwtPayload
    return this.userService.findById(userJwt.id)
  }

  @AuthPost('/:id')
  updateUser(@Param('id') id, @Body() userDto: UpdateUserDto) {
    return this.userService.updateUser(id, userDto)
  }
}