import { Component, Inject } from '@nestjs/common'
import Providers from '../constants/providers'
import { DefaultTransactionRepository } from '../repositories/transaction.repository'
import TransactionModel from '../entities/transaction.entity'
import { CreateTransactionDto, RequestNewTransactionDto } from '../dto/transaction.dto'
import TransactionStatuses from '../constants/transaction-statuses'
import UserService from './user.service'

@Component()
export default class DefaultTransactionService {

  constructor(@Inject(Providers.transactionRepository) private readonly transactionRepository: DefaultTransactionRepository,
              private readonly userService: UserService) {
  }

  findByUserId(userId: number): Promise<TransactionModel[]> {
    return this.transactionRepository.getTransactionsByUserId(userId)
  }

  async requestNewTransaction({ amount, payoutType, userId }: RequestNewTransactionDto) {
    return this.transactionRepository.createNewTransaction({
                                                             user: await this.userService.findById(userId),
                                                             amount,
                                                             payoutType,
                                                             isAuto: false,
                                                             status: TransactionStatuses.requested,
                                                             date: new Date()
                                                           })
  }

  async createNewTransaction({ amount, payoutType, userId }: CreateTransactionDto) {
    return this.transactionRepository.createNewTransaction({
                                                             amount,
                                                             payoutType,
                                                             user: await this.userService.findById(userId),
                                                             isAuto: false,
                                                             status: TransactionStatuses.inProgress,
                                                             date: new Date()
                                                           })
  }
}