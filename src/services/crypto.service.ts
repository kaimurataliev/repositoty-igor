import * as c from 'config'
import { Component } from '@nestjs/common'
import DefaultCalculatingService from './calculating.service'
import BaseWebsocketService from './base/websocket.service'


@Component()
export default class CryptoService extends BaseWebsocketService {
  constructor(private readonly calculatingService: DefaultCalculatingService) {
    super(c.get('crypto-websockets.url'))
    this.emit('SubAdd', { subs: c.get('crypto-websockets.subscriptions') })
    this.on('m', (message) => this.handleOnMessage(message))
  }

  handleOnMessage(message) {
    this.calculatingService.calculate(message)
  }
}
