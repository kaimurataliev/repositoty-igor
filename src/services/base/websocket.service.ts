const io = require('socket.io-client')

export default class BaseWebsocketService {
  socket: any

  constructor(url) {
    this.socket = io(url)
  }

  emit(eventName, ...args) {
    this.socket.emit(eventName, ...args)
  }

  on(eventName, callback) {
    this.socket.on(eventName, callback)
  }

}