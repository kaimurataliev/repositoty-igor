import { Component, HttpException, NotFoundException, UnauthorizedException } from '@nestjs/common'
import UserService from './user.service'
import { comparePasswords, getJwtPayload, hashPassword, signJwt } from '../utils/authentication.util'
import { UserAuthenticateDto, UserRegisterDto } from '../dto/user.dto'
import PgErrors from '../constants/pg-errors'


const USER_CODE_HANDLER_STRATEGY = [
  {
    filter: (code) => code === PgErrors.integrityConstraintViolation.UNIQUE_VIOLATION,
    getErrorMessage: (): string => 'User with this email already exists'
  },
  {
    filter: () => true,
    getErrorMessage: (): string => 'Unknown error'
  }
]

@Component()
export default class DefaultAuthenticationService {

  constructor(private readonly userService: UserService) {
  }

  async register(userRegisterDto: UserRegisterDto): Promise<string> {
    const { password } = userRegisterDto
    const hashedPassword = await hashPassword(password)
    try {
      const user = await this.userService.createNewUser({ ...userRegisterDto, password: hashedPassword })
      return signJwt(getJwtPayload(user))
    } catch (e) {
      const { getErrorMessage } = USER_CODE_HANDLER_STRATEGY.find(it => it.filter(e.code))
      throw new HttpException(getErrorMessage(), 400)
    }
  }

  async authenticate({ email, password }: UserAuthenticateDto): Promise<string> {
    const user = await this.userService.findByEmail(email)
    if (!user) {
      throw new NotFoundException(`User with email ${email} wasn't found`)
    }
    const isPasswordValid = await comparePasswords(password, user.password)
    if (!isPasswordValid) {
      throw new UnauthorizedException('Wrong combination of email and password')
    }
    return signJwt(getJwtPayload(user))
  }
}