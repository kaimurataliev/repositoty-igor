import { Component, Inject } from '@nestjs/common'
import UserModel from '../entities/user.entity'
import { DefaultUserRepository } from '../repositories/user.repository'
import Providers from '../constants/providers'
import { UpdateUserDto, UserRegisterDto } from '../dto/user.dto'

@Component()
export default class DefaultUserService {

  constructor(@Inject(Providers.userRepository) private readonly userRepository: DefaultUserRepository) {
  }

  async findById(id: number): Promise<UserModel> {
    return await this.userRepository.findById(id)
  }

  findByEmail(email: string): Promise<UserModel> {
    return this.userRepository.findByEmail(email)
  }

  async createNewUser(user: UserRegisterDto) {
    return this.userRepository.createNewUser(user)
  }

  async updateUser(id, userDto: UpdateUserDto) {
    return this.userRepository.updateUser(id, userDto)
  }
}