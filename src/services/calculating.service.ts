import { Component } from '@nestjs/common'

@Component()
export default class DefaultCalculatingService {

  calculate(data) {
    console.log(data)
    return data
  }
}