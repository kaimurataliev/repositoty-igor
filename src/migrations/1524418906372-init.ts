import { MigrationInterface, QueryRunner } from 'typeorm'
import UserRoles from '../constants/user-roles'

export class init1524418906372 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`
      CREATE TABLE "user"
        (
          id                 SERIAL              NOT NULL
            CONSTRAINT "PK_cace4a159ff9f2512dd42373760"
            PRIMARY KEY,
          email              VARCHAR(150)        NOT NULL
            CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22"
            UNIQUE,
          password           VARCHAR             NOT NULL,
          power              INTEGER,
          balance            NUMERIC,
          pincode            INTEGER,
          avatar             VARCHAR,
          role               VARCHAR DEFAULT '${UserRoles.USER}',
          "autoPayoutAmount" INTEGER DEFAULT 100 
        );
        
      CREATE TABLE transaction
        (
          id           SERIAL                   NOT NULL
            CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab9"
            PRIMARY KEY,
          amount       NUMERIC                  NOT NULL,
          date         TIMESTAMP WITH TIME ZONE NOT NULL,
          status       VARCHAR                  NOT NULL,
          "isAuto"     BOOLEAN                  NOT NULL,
          "payoutType" VARCHAR                  NOT NULL,
          "userId"     INTEGER
            CONSTRAINT "FK_605baeb040ff0fae995404cea37"
            REFERENCES "user"
        ); 
      `)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
  }

}
