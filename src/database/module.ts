import { Module } from '@nestjs/common'
import { databaseProviders } from './providers'

@Module({
          components: [...databaseProviders],
          exports: [...databaseProviders],
        })
export class DatabaseModule {}