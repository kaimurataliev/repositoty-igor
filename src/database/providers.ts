import { ConnectionOptions, createConnection } from 'typeorm'
import * as c from 'config'
import Providers from '../constants/providers'

const typeOrmConfigs: ConnectionOptions = {
  type: 'postgres',
  host: c.get('db.hostname'),
  port: c.get('db.port'),
  username: c.get('db.username'),
  password: c.get('db.password'),
  database: c.get('db.database'),
  entities: ['src/entities/*.entity.ts'],
  migrationsRun: true,
  migrations: ['src/migrations/*.ts'],
  logging: c.get('db.logging')
}

export const databaseProviders = [
  {
    provide: Providers.dbConnection,
    useFactory: async () => await createConnection(typeOrmConfigs)
  }
]