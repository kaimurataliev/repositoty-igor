import { MiddlewaresConsumer, Module, NestModule } from '@nestjs/common'
import { providers } from './repositories/repositories.providers'
import { DatabaseModule } from './database/module'
import { JwtStrategy } from './utils/jwt.strategy'
import { AuthenticationController } from './controllers/authentication.controller'
import DefaultAuthenticationService from './services/authentication.service'
import DefaultUserService from './services/user.service'
import { UserController } from './controllers/user.controller'
import { transformAuthRoutes } from './utils/authentication.util'
import * as passport from 'passport'
import { TransactionController } from './controllers/transaction.controller'
import DefaultTransactionService from './services/transaction.service'
import CryptoService from './services/crypto.service'
import DefaultCalculatingService from './services/calculating.service'

@Module({
          imports: [DatabaseModule],
          controllers: [
            AuthenticationController,
            UserController,
            TransactionController
          ],
          components: [
            ...providers,
            JwtStrategy,
            DefaultAuthenticationService,
            DefaultUserService,
            DefaultTransactionService,
            DefaultCalculatingService,
            CryptoService
          ]
        })
export class ApplicationModule implements NestModule {

  public configure(consumer: MiddlewaresConsumer) {
    if (!global.authRoutes) {
      return
    }
    const authRoutes = transformAuthRoutes(global.authRoutes)
    consumer
      .apply((passport.authenticate('jwt', { session: false })))
      .forRoutes(...authRoutes)
  }
}
